<?php
/*
 *   *********************************************************************************************
 *      Please retain this copyright header in all versions of the software.
 *      Bitte belassen Sie diesen Copyright-Header in allen Versionen der Software.
 *
 *      Copyright (C) Josef A. Puckl | eComStyle.de
 *      All rights reserved - Alle Rechte vorbehalten
 *
 *      This commercial product must be properly licensed before being used!
 *      Please contact info@ecomstyle.de for more information.
 *
 *      Dieses kommerzielle Produkt muss vor der Verwendung ordnungsgemäß lizenziert werden!
 *      Bitte kontaktieren Sie info@ecomstyle.de für weitere Informationen.
 *   *********************************************************************************************
 */

namespace Ecs\UStIDPflichtInEU\Core;

use \OxidEsales\Eshop\Core\Registry;

class InputValidator extends InputValidator_parent
{
    public function checkVatId($oUser, $aInvAddress)
    {
        $missthevat = false;
        $oConfig    = Registry::getConfig();

        if ($oConfig->getConfigParam('ecs_ustidpfl_eu')) {
            $oCountry = $this->_getCountry($aInvAddress['oxuser__oxcountryid']);
            if ($oCountry->isInEU() && $oCountry->isForeignCountry() && !$aInvAddress['oxuser__oxustid']) {
                $missthevat = true;
            }
        }

        if ($param = $oConfig->getConfigParam('ecs_ustidpfl_param')) {
            if (!$oConfig->getRequestParameter($param) && !$aInvAddress['oxuser__oxustid']) {
                $missthevat = true;
            }
        }

        if ($missthevat) {
            $exception = oxNew(\OxidEsales\Eshop\Core\Exception\InputException::class);
            $exception->setMessage(\OxidEsales\Eshop\Core\Registry::getLang()->translateString('ECS_EUVATIDMISS'));
            return $this->_addValidationError("oxuser__oxustid", $exception);
        }

        return parent::checkVatId($oUser, $aInvAddress);
    }
}
