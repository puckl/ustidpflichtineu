<?php
/*
 *   *********************************************************************************************
 *      Please retain this copyright header in all versions of the software.
 *      Bitte belassen Sie diesen Copyright-Header in allen Versionen der Software.
 *
 *      Copyright (C) Josef A. Puckl | eComStyle.de
 *      All rights reserved - Alle Rechte vorbehalten
 *
 *      This commercial product must be properly licensed before being used!
 *      Please contact info@ecomstyle.de for more information.
 *
 *      Dieses kommerzielle Produkt muss vor der Verwendung ordnungsgemäß lizenziert werden!
 *      Bitte kontaktieren Sie info@ecomstyle.de für weitere Informationen.
 *   *********************************************************************************************
 */

$sMetadataVersion   = '2.0';
$aModule            = [
    'id'            => 'ecs_ustidineu',
    'title'         => '<strong style="color:#04B431;">e</strong><strong>ComStyle.de</strong>:  <i>USt-ID-Pflicht fuer EU-Laender</i>',
    'description'   => '<i>Die Angabe der USt-ID ist bei EU-Bestellungen Pflicht, jedoch nicht bei Bestellungen aus dem Heimatland.</i>',
    'version'       => '2.0.1',
    'thumbnail'     => 'ecs.png',
    'author'        => '<strong style="font-size: 17px;color:#04B431;">e</strong><strong style="font-size: 16px;">ComStyle.de</strong>',
    'email'         => 'info@ecomstyle.de',
    'url'           => 'https://ecomstyle.de',
    'extend' => [
        \OxidEsales\Eshop\Core\InputValidator::class => Ecs\UStIDPflichtInEU\Core\InputValidator::class,
    ],
    'settings' => [
        ['group' => 'ecs_main', 'name' => 'ecs_ustidpfl_param',     'type' => 'str',    'value' => ''],
        ['group' => 'ecs_main', 'name' => 'ecs_ustidpfl_eu',        'type' => 'bool',   'value' => false],
    ],
    'events' => [
        'onActivate'    => 'Ecs\UStIDPflichtInEU\Core\Events::onActivate',
        'onDeactivate'  => 'Ecs\UStIDPflichtInEU\Core\Events::onDeactivate',
    ],
];
