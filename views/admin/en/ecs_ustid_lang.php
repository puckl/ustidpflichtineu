<?php
/*
 *   *********************************************************************************************
 *      Please retain this copyright header in all versions of the software.
 *      Bitte belassen Sie diesen Copyright-Header in allen Versionen der Software.
 *
 *      Copyright (C) Josef A. Puckl | eComStyle.de
 *      All rights reserved - Alle Rechte vorbehalten
 *
 *      This commercial product must be properly licensed before being used!
 *      Please contact info@ecomstyle.de for more information.
 *
 *      Dieses kommerzielle Produkt muss vor der Verwendung ordnungsgemäß lizenziert werden!
 *      Bitte kontaktieren Sie info@ecomstyle.de für weitere Informationen.
 *   *********************************************************************************************
 */

$sLangName = 'English';
$aLang     = [
    'charset'                           => 'UTF-8',
    'SHOP_MODULE_GROUP_ecs_main'        => 'Settings',
    'SHOP_MODULE_ecs_ustidpfl_param'    => 'UStID ist Pflicht, wenn Parameter nicht gesetzt.',
    'SHOP_MODULE_ecs_ustidpfl_eu'       => 'UStID ist Pflicht, wenn EU Land.',

];
